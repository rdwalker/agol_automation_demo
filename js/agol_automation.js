/**
 * R. Dylan Walker
 * dwalker@geosyntec.com
 * 19 February 2018
 */

// Load resources
require([
    "esri/Map",
    "esri/views/MapView",
    "esri/Graphic",
    "esri/layers/FeatureLayer",
    "esri/widgets/ScaleBar",
    "esri/widgets/Legend",
    "dojo/domReady!"], function (Map, MapView, Graphic, FeatureLayer, ScaleBar, Legend) {


    //Scoped globals
    let layer, view = null;
    /**
     * Function to create a view in the given container of the give map with center coordinates and scale
     * @param container DOM element to put the view into
     * @param map ESRI Map to use for the view
     * @param center Center of the map given as a two value list; [long, lat]
     * @param scale Scale of the map given as a value (e.g., 1:24000 would be scale = 24000)
     * @param zoom Zoom level of the map given as integer between 1 and 12
     * @returns {object} ESRI MapView
     */
    const createView = (container, map, center, scale = null, zoom = null) => {
        let view = new MapView({
            container: container,
            map: map,
            center: center,
        });
        if (scale) {
            view.scale = scale
        }
        if (zoom) {
            view.zoom = zoom
        }
        //Check error on view
        view.when(() => {
        }, (error) => {
            console.log("The view resources failed to load: ", error)
        });
        return view;
    };

    /**
     * Initialization function
     */
    const init = () => {
        // Create a map used for selection of location
        let MyMap = new Map({
            basemap: 'topo'
        });

        //Configure symbols
        const [fields, ptRenderer] = configure_symbols();

        // configuration view of the map
        view = createView("map", MyMap, [-85, 38], null, 3);
        //attached click to map to get coordinates
        view.on("click", (evt) => {
            $('#point_modal').addClass("open");
            $('#mapPoint').val(evt.mapPoint);

            $('#point_data').submit((subevt) => {
                subevt.preventDefault();
                subevt.stopImmediatePropagation();
                add_point(MyMap, $('#mapPoint').val(), fields, ptRenderer);
            });
        });

        // Remove loading modal after views have finished
        view.when(() => {
            add_scalebar(view);
            document.getElementsByTagName("body")[0].setAttribute("class", "");
        });

        // Email handler
        $('#form_email').submit((evt) => {
            evt.preventDefault();
            generate_email();
        });
        //Close Modal
        $(".modal").removeClass("open");
    };
    /**
     * Create scalebar
     * @param view
     */
    const add_scalebar = (view) => {
        /**
         * Creates an ESRI Scalebar in the given view         *
         */
        let scalebar = new ScaleBar({
            view: view,
            style: "ruler",
            units: "feet",
        });
        view.ui.add(scalebar, {position: "bottom-left"});
    };

    /**
     * Configure symbols for use in the map
     * @returns {*[]}
     */
    const configure_symbols = () => {
        // Feature fields
        const fields = [
            {name: "ObjectID", alias: "ObjectID", type: "oid"},
            {name: "LocationID", alias: "Location ID", type: "string"},
            {name: "LocationType", alias: "Location Type", type: "string"}
        ];

        // Point Configuration
        // graphic renderer options
        const ptRenderer = {
            type: "unique-value",
            field: "LocationType",
            // defaultSymbol: {
            //     type: "simple-marker",
            //     color: "black"
            // },
            // defaultLabel: "Locations",
            uniqueValueInfos: [
                {
                    value: "soil",
                    label: "Soil",
                    symbol: {
                        type: "simple-marker",
                        color: "red"
                    }
                },
                {
                    value: "groundwater",
                    label: "Groundwater",
                    symbol: {
                        type: "simple-marker",
                        color: "blue"
                    }
                }
            ]
        };
        return [fields, ptRenderer];
    };
    /**
     * Create legend; must be done after creation of layer or will show no legend; could destroy and re-create if layer changed
     * @param view
     */
    const add_legend = (view) => {
        let legend = new Legend({
            view: view,
            layerInfos: [{
                layer: layer
            }]
        });

        view.ui.add(legend, "bottom-right");
    };

    /**
     * Create a point on the map
     * @param map
     * @param mapPoint
     * @param fields
     * @param ptRenderer
     */
    const add_point = (map, mapPoint, fields, ptRenderer) => {
        /**
         * Asks user for input data, if they submit then
         * Adds a new point to the given map at the given latitude and longtitude
         */

        let locid = $('#input_loc').val();
        let loctype = $("#input_type2")[0].checked ? "soil" : "groundwater";

        // Create graphic to add to map
        let point = {
            geometry: mapPoint,
            attributes: {
                LocationID: locid,
                LocationType: loctype
            }
        };

        // If layer already exists, add to it, otherwise make one
        if (layer) {
            layer.source.add(point);
        } else {
            layer = new FeatureLayer({
                source: [point],
                geometryType: "point",
                fields: fields,
                objectIdField: "ObjectID",
                displayField: "LocationID",
                renderer: ptRenderer,
                supportsAdd: true
            });
            map.add(layer);
            add_legend(view);
        }

        //Clear inputs
        $('#input_loc').val(null);
        $('#input_type1')[0].checked = false;
        $('#input_type2')[0].checked = false;
        $('#point_modal').removeClass("open");
    };

    const generate_email = () => {

        let htmlDocument = "<h2>AGOL Demo Data</h2>";
        htmlDocument += "<p>This is a <strong>simple</strong> example e-mail that could be sent to users of ArcGIS " +
            "Online Applications.  These documents can be more advanced and include attachments.</p>";
        htmlDocument += "<table cellpadding='5'><th>Location ID</th><th>Location Type</th><th>Latitude</th><th>Longitude</th>";
        layer.source.forEach((k, v) => {
            let attr = k.attributes;
            let geo = k.geometry;
            htmlDocument += '<tr><td>' + attr.LocationID + '</td>' +
                '<td>' + attr.LocationType + '</td>' +
                '<td>' + geo.latitude + '</td>' +
                '<td>' + geo.longitude + '</td>' +
                '</tr>'
        });
        htmlDocument += "</table>";

        let emlContent = "data:message/rfc822 eml;charset=utf-8,";
        emlContent += 'To: ' + $('#input_email').val() + '\n';
        emlContent += 'Subject: AGOL Automation Demo Data\n';
        emlContent += 'X-Unsent: 1' + '\n';
        emlContent += 'Content-Type: text/html' + '\n';
        emlContent += '' + '\n';
        emlContent += '<!DOCTYPE html><html><head>' +
            '<style>table, td, th {border: 1pt black solid; border-collapse: collapse}</style>' +
            '</head><body>' + htmlDocument + '</body></html>';

        let encodedUri = encodeURI(emlContent); //encode spaces etc like a url
        let a = document.createElement('a'); //make a link in document
        let linkText = document.createTextNode("fileLink");
        a.appendChild(linkText);
        a.href = encodedUri;
        a.id = 'fileLink';
        a.download = 'AGOL_Demo.eml';
        a.style = "display:none;"; //hidden link
        document.body.appendChild(a);
        document.getElementById('fileLink').click(); //click the link
    };

    // Show loading modal
    document.getElementsByTagName("body")[0].setAttribute("class", "loading");

    // Start
    init();

})
;

//Handle form reset
const reset_form = () => {
    $('#point_modal').removeClass('open');
    $('#input_loc').val(null);
    $('#input_type1')[0].checked = false;
    $('#input_type2')[0].checked = false;
};

